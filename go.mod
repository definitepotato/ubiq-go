module gitlab.com/ubiqsecurity/ubiq-go

go 1.18

require (
	github.com/go-ini/ini v1.67.0
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a
	gitlab.com/ubiqsecurity/ubiq-fpe-go v0.0.0-20231208163039-a332b3d2231b
)

require (
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d // indirect
)
